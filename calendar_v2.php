<!DOCTYPE html>
<html lang="en-US">
<meta charset="UTF-8">
<title>a Calendar</title>
<link rel="stylesheet" href="calendar_style_layout.css" />
<script src="send.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

<body>
    <?php
        ini_set("session.cookie_httponly", 1);
        session_start();

        $previous_ua = @$_SESSION['useragent'];
        $current_ua = $_SERVER['HTTP_USER_AGENT'];

        if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
	        die("Session hijack detected");
        }else{
	        $_SESSION['useragent'] = $current_ua;
        }

    ?>
    Username: <input type="text" id="username" />
    Password: <input type="password" id="password" />
    <button id="login_btn" class="button">Log In</button>
    <script src="ajax.js"></script>
    <button id="logout_btn" class="logout">Log Out</button>
    <script src="ajax_logout.js"></script>

    <h3>A Simple Calendar</h3>

    <!--Cycle through the months-->
    <div id="otherMonths">
        <button id="prev" class="button"> Prev Month </button>
        <button id="next" class="button"> Next Month </button>
    </div>

    <div id="demoMonthandYear"></div>
    <div id="demoWeekdays"></div>
    <div id="demoDays"></div>
    <form name="print">
        <input type="button" class="button" value="Print" id="button4" onclick="window.print();">
    </form>

    <!-- <ul class="days">  
      <li>1</li>
      <li><span class="active">10</span></li>
    </ul> -->
    <h4>Add an Event</h4>
    <form name="newEvent" method="POST" id="event">
        Name: <input type="text" name="name" id="name" placeholder="Ex: Birthday"><br/>
        Day: <input type="number" name="day" id="day">
        Month: <input type="number" name="month" id="month">
        Year: <select name="year" id="year">
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
        </select><br/>
        Time: <input type="text" name="time" id="time"><br/>
        Category: <select name="category" id="category">
            <option value="none">None</option>
            <option value="class">Class</option>
            <option value="work">Work</option>
            <option value="birthday">Birthday</option>
            <option value="appointment">Concert</option>
            <option value="holiday">Holiday</option>
        </select><br/>
        Event Description (optional): <br/><textarea rows="3" cols="30" name ="description" id="description"></textarea><br/>
        Share Event With: <input type="text" name="shareWith" id="shareWith" placeholder="Ex: JohnDoe123"><br/>
        <input type="button" class="button" value="Create" id="button" onclick="SendData(); ShowBubble();">
        <input type="button" class="button" value="Edit" id="button2" onclick="EditData();">
        <br/><br/>
        Share my Calendar with: <input type="text" name="shareUsr" id="shareUsr" class="shareCal" placeholder="Enter User">
        <input type="button" class="button" value="Share" id="button3" onclick="ShareIt();">
    </form>
    <div id="results"></div>


    <!--From wiki through line 93-->
    <script>
        li_array = [];
            (function () {
                "use strict";
                //Day object
                Date.prototype.deltaDays = function (n) {
                    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + n);
                };
                //gets sunday before your day
                Date.prototype.getSunday = function () {
                    return this.deltaDays(-1 * this.getDay());
                };
            }());
    
            //a_week = new Week(a_day);
            //gives a week of days starting from the sunday object thru 68
            function Week(initial_d) {
                "use strict";
            
                this.sunday = initial_d.getSunday();
                
                this.nextWeek = function () {
                    return new Week(this.sunday.deltaDays(7));
                };
                this.prevWeek = function () {
                    return new Week(this.sunday.deltaDays(-7));
                };
                this.contains = function (d) {
                    return (this.sunday.valueOf() === d.getSunday().valueOf());
                };
                this.getDates = function () {
                    var dates = [];
                    for(var i=0; i<7; i++){
                        dates.push(this.sunday.deltaDays(i));
                    }
                    return dates;
                };
            }
     
            //takes a series of  weeks and creates a list of lists (list of weeks, which are lists of days)
            function Month(year, month) {
                "use strict";
                this.year = year;
                this.month = month;
                
                this.nextMonth = function () {
                    return new Month( year + Math.floor((month+1)/12), (month+1) % 12);
                };
                this.prevMonth = function () {
                    return new Month( year + Math.floor((month-1)/12), (month+11) % 12);
                };
                this.getDateObject = function(d) {
                    return new Date(this.year, this.month, d);
                };
                this.getWeeks = function () {
                    var firstDay = this.getDateObject(1);
                    var lastDay = this.nextMonth().getDateObject(0);
                    
                    var weeks = [];
                    var currweek = new Week(firstDay);
                    weeks.push(currweek);
                    while(!currweek.contains(lastDay)){
                        currweek = currweek.nextWeek();
                        weeks.push(currweek);
                    }
                    return weeks;
                };
            }
            
            //Ben code
            const currDay = new Date();
            const currYear = currDay.getFullYear();
            const currMonth = currDay.getMonth();

            function getTheMonth(currYear, currMonth){
                currDates = new Month(currYear, currMonth);
                //this object contains weeks, lists of lists, lists of weeks

                //alert(currDatess.nextMonth().month);
                //alert(currDatess.getWeeks()[0].getDates());
                
                //appends demo month and year
                let div_1 = document.createElement("div");
                div_1.appendChild(document.createTextNode((currDates.month+1).toString()));
                div_1.appendChild(document.createTextNode(" / "));
                div_1.appendChild(document.createTextNode(currDates.year.toString()));
                div_1.setAttribute("class", "month&year");
                document.getElementById("demoMonthandYear").appendChild(div_1);
    
                //appends demo weekdays thru 147
                let Sun = document.createElement("LI");
                Sun.appendChild(document.createTextNode("Sun"));
                Sun.setAttribute("class", "Sun");
                document.getElementById("demoWeekdays").appendChild(Sun);
    
                let Mon = document.createElement("LI");
                Mon.appendChild(document.createTextNode("Mon"));
                Mon.setAttribute("class", "Mon");
                document.getElementById("demoWeekdays").appendChild(Mon);
    
                let Tue = document.createElement("LI");
                Tue.appendChild(document.createTextNode("Tue"));
                Tue.setAttribute("class", "Tue");
                document.getElementById("demoWeekdays").appendChild(Tue);
    
                let Wed = document.createElement("LI");
                Wed.appendChild(document.createTextNode("Wed"));
                Wed.setAttribute("class", "Wed");
                document.getElementById("demoWeekdays").appendChild(Wed);
    
                let Thur = document.createElement("LI");
                Thur.appendChild(document.createTextNode("Thur"));
                Thur.setAttribute("class", "Thur");
                document.getElementById("demoWeekdays").appendChild(Thur);
    
                let Fri = document.createElement("LI");
                Fri.appendChild(document.createTextNode("Fri"));
                Fri.setAttribute("class", "Fri");
                document.getElementById("demoWeekdays").appendChild(Fri);
    
                let Sat = document.createElement("LI");
                Sat.appendChild(document.createTextNode("Sat"));
                Sat.setAttribute("class", "Sat");
                document.getElementById("demoWeekdays").appendChild(Sat);
    
    
                div_array = [];
                for (var i=0; i<currDates.getWeeks().length; i++){
                    for (var j=0; j<currDates.getWeeks()[i].getDates().length; j++){
                        div_array.push(currDates.getWeeks()[i].getDates()[j]);
                    }
                }
                var sample=currMonth+1
                //creates li's in the dom
                // li_array = [];
                if (sample==1){dayNum=1;}
                else if (sample==2){dayNum=4;}
                else if (sample==3){dayNum=4;}
                else if (sample==4){dayNum=0;}
                else if (sample==5){dayNum=2;}
                else if (sample==6){dayNum=5;}
                else if (sample==7){dayNum=0;}
                else if (sample==8){dayNum=3;}
                else if (sample==9){dayNum=-1;}
                else if (sample==10){dayNum=1;}
                else if (sample==11){dayNum=4;}
                else if (sample==12){dayNum=-1;}
                for (var k=0; k<div_array.length; k++){
                    //console.log(div_array[k].getDate());
                    li_array[k] = document.createElement("li");
                    // const url='display_events.php?data='.concat(k).'&data2='.concat(currMonth)
                    const calculate=k-dayNum;
                    const url='display_events.php?data='.concat(calculate, '&data2=', sample)
                    const goHere="window.open('".concat(url, "', 'popup', 'width=600,height=300'); return false;")
                    const theDay=calculate.toString();
                    const nextMonth=currMonth+1
                    const theMonth=nextMonth.toString();
                    const theClass=SetClass(theDay, theMonth);
                    li_array[k].setAttribute("onclick", goHere);
                    li_array[k].setAttribute("class", theClass)
                    // li_array[k].setAttribute("onclick", "window.open('display_events.php?data=', 'popup', 'width=600,height=300'); return false;")
                    li_array[k].setAttribute("id", "clickDay")
                    li_array[k].setAttribute("name", k)
                    li_array[k].appendChild(document.createTextNode(div_array[k].getDate().toString()));
                    document.getElementById("demoDays").appendChild(li_array[k]);
                }
            }
    

            document.addEventListener("DOMContentLoaded", getTheMonth(currYear, currMonth), false);    
 
            document.getElementById("prev").addEventListener("click", function(){
                let monthYear = document.getElementById("demoMonthandYear").getElementsByClassName("month&year")[0];
                document.getElementById("demoMonthandYear").removeChild(monthYear);
                let weekDays = document.getElementById("demoWeekdays").childNodes;
                const i = 0;
                while(weekDays[i]){
                    document.getElementById("demoWeekdays").removeChild(weekDays[i]);
                };
                let Days = document.getElementById("demoDays").childNodes;
                const j = 0;
                while(Days[j]){
                    document.getElementById("demoDays").removeChild(Days[j]);
                };
                this.currDay = new Date(currDay.setMonth(currDay.getMonth() - 1)); //Referenced https://stackoverflow.com/questions/26488043/javascript-get-previous-months-date/26488640
                this.currMonth = this.currDay.getMonth();
                this.currYear = this.currDay.getFullYear();
                getTheMonth(this.currYear, this.currMonth);
            }, false);
    
            document.getElementById("next").addEventListener("click", function(){
                let monthYear = document.getElementById("demoMonthandYear").getElementsByClassName("month&year")[0];
                document.getElementById("demoMonthandYear").removeChild(monthYear);
                let weekDays = document.getElementById("demoWeekdays").childNodes;
                const i = 0;
                while(weekDays[i]){
                    document.getElementById("demoWeekdays").removeChild(weekDays[i]);
                };
                let Days = document.getElementById("demoDays").childNodes;
                const j = 0;
                while(Days[j]){
                    document.getElementById("demoDays").removeChild(Days[j]);
                };
                this.currDay = new Date(currDay.setMonth(currDay.getMonth() + 1)); //Referenced https://stackoverflow.com/questions/26488043/javascript-get-previous-months-date/26488640
                this.currMonth = this.currDay.getMonth();
                this.currYear = this.currDay.getFullYear();
                getTheMonth(this.currYear, this.currMonth);
            }, false);

            function SetClass(theDay, theMonth){
                let newDay="";
                let newMonth="";
                if(theDay=='1'){newDay="one"}
                else if (theDay=='2'){newDay="two"}
                else if (theDay=='3'){newDay="three"}
                else if (theDay=='4'){newDay="four"}
                else if (theDay=='5'){newDay="five"}
                else if (theDay=='6'){newday="six"}
                else if (theDay=='7'){newDay="seven"}
                else if (theDay=='8'){newDay="eight"}
                else if (theDay=='9'){newDay="nine"}
                else if (theDay=='10'){newDay="ten"}
                else if (theDay=='11'){newDay="eleven"}
                else if (theDay=='12'){newDay="twelve"}
                else if (theDay=='13'){newDay="thirteen"}
                else if (theDay=='14'){newDay="fourteen"}
                else if (theDay=='15'){newDay="fifteen"}
                else if (theDay=='16'){newDay="sixteen"}
                else if (theDay=='17'){newDay="seventeen"}
                else if (theDay=='18'){newDay="eighteen"}
                else if (theDay=='19'){newDay="nineteen"}
                else if (theDay=='20'){newDay="twenty"}
                else if (theDay=='21'){newDay="twentyone"}
                else if (theDay=='22'){newDay="twentytwo"}
                else if (theDay=='23'){newDay="twentythree"}
                else if (theDay=='24'){newDay="twentyfour"}
                else if (theDay=='25'){newDay="twentyfive"}
                else if (theDay=='26'){newDay="twentysix"}
                else if (theDay=='27'){newDay="twentyseven"}
                else if (theDay=='28'){newDay="twentyeight"}
                else if (theDay=='29'){newDay="twentynine"}
                else if (theDay=='30'){newDay="thirty"}
                else if (theDay=='31'){newDay="thirtyone"}
                if (theMonth=='1'){newMonth="one"}
                else if (theMonth=='2'){newMonth="two"}
                else if (theMonth=='3'){newMonth="three"}
                else if (theMonth=='4'){newMonth="four"}
                else if (theMonth=='5'){newMonth="five"}
                else if (theMonth=='6'){newMonth="six"}
                else if (theMonth=='7'){newMonth="seven"}
                else if (theMonth=='8'){newMonth="eight"}
                else if (theMonth=='9'){newMonth="nine"}
                else if (theMonth=='10'){newMonth="ten"}
                else if (theMonth=='11'){newMonth="eleven"}
                else if (theMonth=='12'){newMonth="twelve"}
                return newDay.concat(newMonth);
            }
            function ShowBubble(){
                var category=$("#category").val();
                var month=$("#month").val();
                var day=$("#day").val();
                // category=toString(category);
                var color;
                if (category=='None'){
                    color="grey";
                } else if (category=='class'){
                    color="blue"
                } else if (category=='work'){
                    color="red";
                } else if (category=='concert'){
                    color="white";
                } else if (category=='birthday'){
                    color="yellow";
                } else if (category=='holiday'){
                    color="green";
                }
                
                theDay=day.toString();
                theMonth=month.toString();
                let finalClass = SetClass(theDay, theMonth)

                // const edit = document.createElement("LI")
                // // const edit = document.getElementById('demoDays').getElementsByClassName(finalClass)[0];
                // edit.setAttribute('class', 'event');
                // document.getElementById('demoDays').getElementsByClassName(finalClass).appendChild(edit);
            }
            
    </script>
</body>
</html>