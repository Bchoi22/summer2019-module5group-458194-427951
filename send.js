//this code is modeled after code found at https://tecadmin.net/submit-form-without-page-refresh-php-jquery/ that explains
//how to submit a form with out refreshing the page and also carry out php action
function SendData(){
    var name=$("#name").val();
    var day=$("#day").val();
    var month=$("#month").val();
    var year=$("#year").val();
    var time=$("#time").val();
    var category=$("#category").val();
    var description=$("#description").val();
    shareWith=$("#shareWith").val();
    $.post("submit.php", {name:name, day:day, month:month, year:year, time:time, category:category, description:description, shareWith:shareWith},
    function(data){
        $('#results').html(data);
        $('#event')[0].reset();
    })
    // li_array[day] = document.getElementById('demoDays');
    // li_array[day].setAttribute('class', 'day');
    // document.getElementById('demoDays').appendChild(li_array[day]);
}
function EditData(){
    var name=$("#name").val();
    var day=$("#day").val();
    var month=$("#month").val();
    var year=$("#year").val();
    var time=$("#time").val();
    var category=$("#category").val();
    var description=$("#description").val();
    $.post("editEvent.php", {name:name, day:day, month:month, year:year, time:time, category:category, description:description},
    function(data){
        $('#results').html(data);
        $('#event')[0].reset();
    })
}
function ShareIt(){
    var name=$("#name").val();
    shareUsr=$("#shareUsr").val();
    $.post("shareCal.php", {name:name, shareUsr:shareUsr},
    function(data){
        $('#results').html(data);
        $('#event')[0].reset();
    })
}