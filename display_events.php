<!DOCTYPE html>
<html>
    <head>
        <title>Displaying your Events</title>
        <link rel="stylesheet" href="calendar_style_layout.css" />
    </head>
    <body>
        <text class="event">Events for the Day:</text><br/>
    <?php
    session_start();
        if(isset($_GET["data"]))
        {
            $data = htmlentities($_GET["data"]);
            $data2=htmlentities($_GET["data2"]);
        }
        $username=htmlentities($_SESSION['username']);

    ?>
    <?php
        require 'database.php';
        $stmt = $mysqli->prepare("select title, month, day, description, category, time, year from Events where user = '$username'");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->execute();
        $stmt->bind_result($title, $month, $day, $description, $category, $time, $year);
        while($stmt->fetch()){
            if ($month==htmlentities($data2)){
            if ($day==htmlentities($data)){
            echo "At ".htmlentities($time).", you have ".htmlentities($title)."<br/>";
            echo "<text class='description'>".htmlentities($description)."</text><br/>";
            echo "<text class='description'> Tag: ".htmlentities($category)."</text>";
            ?>
            <form method="POST" id="delete" action=''>
                <input type='hidden' name='title' value='<?php echo "$title";?>'/>
                <input type="submit" name="delete" value="Delete Event" id="erase"class="logout">
            </form>
            <?php
            }
            }   
        }
    ?>
    <?php
        if (isset($_POST['delete'])){
            $title=htmlentities($_POST['title']);
            require 'database.php';
            $stmt = $mysqli->prepare("delete from Events where title='$title'");
            if(!$stmt){
	        printf("Query Prep Failed: %s\n", $mysqli->error);
	        exit;
            }           
            $stmt->execute();
            $stmt->close();
            echo '<script>window.close();</script>';
        }
    ?>
    </body>
    </html>