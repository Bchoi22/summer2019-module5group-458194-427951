<!DOCTYPE HTML>

<?php
// Referenced https://www.youtube.com/watch?v=FgSysHTsb6A for hints with code using $_SESSION;
// Referenced https://codewithawa.com/posts/check-if-username-is-already-taken-with-php-and-mysql to query db before registering users.
session_start();
require 'database.php';

$first = $mysqli->real_escape_string($_POST['firstname']);
$last = $mysqli->real_escape_string($_POST['lastname']);
$user = $mysqli->real_escape_string($_POST['username']);
$email = $mysqli->real_escape_string($_POST['email']);
$passwordHash = password_hash($_POST['password'], PASSWORD_BCRYPT);
$confirmHash = password_hash($_POST['confirm_password'], PASSWORD_BCRYPT);

$_SESSION['username'] = $user;

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (isset($first) && !empty($first)){
        if (isset($last) && !empty($last)){
            if (isset($user) && !empty($user)){
                $sql_u = "select * from users where username='$user'";
                $res_u = mysqli_query($mysqli, $sql_u);
                if (mysqli_num_rows($res_u) > 0){
                    die('username is taken dude!');
                }else if (isset($email) && !empty($email)){
                    $sql_e = "select * from users where email='$email'";
                    $res_e = mysqli_query($mysqli, $sql_e);
                    if (mysqli_num_rows($res_e) > 0){
                        die('Email address is taken dude!');
                    }else if (password_verify($_POST['password'], $passwordHash) && password_verify($_POST['password'], $confirmHash)){
                        $stmt = $mysqli->prepare("insert into users (first_name, last_name, username, email, password_hash) values (?, ?, ?, ?, ?)");
                        if(!$stmt){
	                        printf("Query Prep Failed: %s\n", $mysqli->error);
	                        exit;
                        }
                        $stmt->bind_param('sssss', $first, $last, $user, $email, $passwordHash);
                        $stmt->execute();
                        $stmt->close();
                        $_SESSION['username'] = $user;
                        echo 'User information added.';
                        //header("Location:welcomepage.php");
                        //session_destroy();
                    }else
                        echo 'Passwords do not match.';
                }else
                    echo 'An email is required.';
            }else
                echo 'A user name is required.';
        }else
            echo 'Please input your last name.';
    }else
        echo 'Please input your first name.';
}else
    echo 'Please input your information to register.';
?>


<html lang="en-US">
<meta charset="UTF-8">
<title>Registration and Login Page</title>
    <body>
        <h3>Registration</h3>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
	        <p>
		        <label for="first_name">First Name:</label>
		        <input type="text" name="firstname" id="firstname" required/>
            </p>
	        <p>
		        <label for="last_name">Last Name:</label>
		        <input type="text" name="lastname" id="lastname" required/>
            </p>
            <p>
		        <label for="username">UserName:</label>
		        <input type="text" name="username" id="username" required/>
            </p>
            <p>
		        <label for="email">Email:</label>
		        <input type="text" name="email" id="email" required/>
            </p>
            <p>
		        <label for="username">Password:</label>
		        <input type="text" name="password" id="password" required/>
            </p>
            <p>
		        <label for="username">Confirm Password:</label>
		        <input type="text" name="confirm_password" id="confirmPassword" required/>
            </p>
	        <p>
		        <input type="submit" value="Register Button" />
	        </p>
        </form><br /><br />

        <h3>Login</h3>
        <form action="login_ajax.php" method="POST">
	        <div>
		        <label for="username">UserName:</label>
                <input type="text" name="username" id="username" required/>
            </div><br />
            <div>
            <label for="pass">Password :</label>
            <input type="password" id="pass" name="password" minlength="8" required/>
            <label for="pass">(8 characters minimum)</label>
	        </div>
	        <p>
		        <input type="submit" value="Login" />
	        </p>
        </form>
    </body>
</html>