function loginAjax(event) {
    const username = document.getElementById("username").value; // Get the username from the form
    const password = document.getElementById("password").value; // Get the password from the form

    // Make a URL-encoded string for passing POST data:
    const data1 = {'username': username, 'password': password};
    
    fetch("login_ajax.php", {
            method: 'POST',
            body: JSON.stringify(data1),
            headers: {'Content-Type': 'application/json'},
            mode: "same-origin",
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(data => alert(data.success ? "You're logged in!" : `You're not logged in: ${data.message}`));

}

document.getElementById("login_btn").addEventListener("click", loginAjax, false); // Bind the AJAX call to button click