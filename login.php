<!DOCTYPE HTML>
<head>
<html lang="en-US">
<meta charset="UTF-8">
<title>Login</title>
<?php
session_start();
require 'database.php';
$user = $mysqli->real_escape_string($_POST['username']);
if (!empty($user)){

    // Use a prepared statement
    $stmt = $mysqli->prepare("select password_hash FROM users WHERE username=?");

    // Bind the parameter
    $stmt->bind_param('s', $user);
    $stmt->execute();

    // Bind the results
    $stmt->bind_result($pwd_hash);
    $stmt->fetch();

    $pwd_inputted = $mysqli->real_escape_string($_POST['password']);

    // Compare the submitted password to the actual password hash
    if(password_verify($pwd_inputted, $pwd_hash)){
        // Login succeeded!
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
        $_SESSION['username'] = $user;
        echo json_encode(array(
            "success" => true
        ));
        // Redirect to your target page
        echo "<script>setTimeout(\"location.href = 'calendar_v3.html';\",1500);</script>";
        //header("Location:calendar_v3.html");
    }else{
        echo json_encode(array(
            "success" => false,
            "message" => "Incorrect Username or Password"
        ));
        exit;
        //die("Login failed.  Please try again or register.");
        //header("Location:logout.php");
    }
}
?>
</html>